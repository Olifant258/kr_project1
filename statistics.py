import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


# Function to open the file
def open_data():
    #We can select 1 line, because each line has the same size.
    #The offset is measured in bytes from the beginning of the file.
    with open('difs.txt', 'r') as f:
        data = f.read()
        data = data.splitlines() # data is a list of strings
        temp = []
        s_data = []
        for x in data:
            temp = x.split()
            s_data.append(temp)
    return s_data

def counter(dat, br, m):
    dist = [0] * ((m / br) + 1)
    for i in dat:
        dist[i / br] += 1
    return dist

def hister(dat, br):
    color = 'bgrcmy'
    m = max([max(x) for x in dat])
    # x_ax = [0:m:br]
    for i in range(len(dat)):
        d = counter(dat[i], br, m)
        plt.plot(d, color[i])
    plt.show()


if __name__ == '__main__':
    # Get data
    data = open_data()
    # Initilize lists
    conflicts = [[],[],[],[]] # List of 6 lists. Each list correspond to one level of difficulty, given by the index + 1 and stores the conflicst values
    cpu = [[],[],[],[]] # List of 6 lists. Each list correspond to one level of difficulty, given by the index + 1 and stores the conflicst values
    l = [1,2,5,6]
    # Read each line and get conflicts and cpu time per difficulty level
    for d in range(len(data)):
        conflicts[l.index(int(data[d][6]))].append(int(data[d][2])) # where 6 is where the difficulty level is stored and 2 where the conflict is stored
        cpu[l.index(int(data[d][6]))].append(float(data[d][5]))  # where 6 is where the difficulty level is stored and 5 where the cpu time is stored
    # Get average and standard deviation per difficulty level
    average_c = [] # list of 6 means, one per difficulty level
    std_c = []
    for c in conflicts:
        average_c.append(np.mean(np.array(c)))
        std_c.append(np.std(np.array(c)))
    print(average_c)
    print(std_c)
    average_cpu = []  # list of 6 means, one per difficulty level
    std_cpu = []
    for c in cpu:
        average_cpu.append(np.mean(np.array(c)))
        std_cpu.append(np.std(np.array(c)))
    print(average_cpu)
    print(std_cpu)

    # hister(conflicts, 10)

    w, p = stats.levene(conflicts[0], conflicts[1], conflicts[2], conflicts[3])
    print(w, p)
    w2, p2 = stats.levene(cpu[1], cpu[3])
    #
    # # Get average and standard deviation for the whole data set
    # # FOR CONFLICTS
    # # Convert conflict to a single list
    # list_c = []
    # for c in conflicts:
    #     for a in c:
    #         list_c.append(a)
    # average_total_c = np.mean(np.array(list_c))
    # std_total_c = np.std(np.array(list_c))
    # print(average_total_c)
    # print(std_total_c)
    #
    # # FOR CPU TIME
    # # Convert conflict to a single list
    # list_cpu = []
    # for c in cpu:
    #     for a in c:
    #         list_cpu.append(a)
    # average_total_cpu = np.mean(np.array(list_cpu))
    # std_total_cpu = np.std(np.array(list_cpu))
    # print(average_total_cpu)
    # print(std_total_cpu)

