from constraints_test import *
import subprocess
import re


# Function to read out the sat solution.
def read_sat():
    # Open the sat output from file.
    with open('C:\\sat\\d2.txt', 'r') as f:
        s = f.read()
    # Process output. We end up with a list of 81 elements.
    # The elements are a list for the values in each row after the other.
    s = s.splitlines()[1]
    s = s.split(' ')[100:-1]
    s = [int(x[2]) for x in s if x[0] is not '-']
    # If we have more or less than 81 values, the solver did not work.
    if len(s) != 81:
        print('Warning: Sudoku not solved correctly.')
    return s


# Function to read STDOUT.
def sat_out(p):
    p = p.splitlines()
    p = [x.split(' ') for x in p]
    p = [[e for e in x if e is not ''] for x in p]
    for i in range(len(p)):
        if p[i][0] == 'restarts':
            break
    p = p[i:]
    del p[7]
    # Put results in list.
    # Format: SATISFIABLE-RESTARTS-CONFLICTS-DECISIONS-PROPS-CPU-SOL
    result = [0] * 8
    if p[-1] == ['SATISFIABLE']:
        result[0] = 1
    result[1:5] = [int(x[2]) for x in p[:-4]]
    result[5] = float(p[6][3])
    return result


# Function to test if the solution is indeed a correct one.
def test_sol(sol, dat):
    # First check that the solution itself is bound to sudoku rules.
    # ROWS
    s = [sol[(i * 9):((i + 1) * 9)] for i in range(9)]
    s = [[x for x in l if l.count(x) > 1] for l in s]
    if s.count([]) != 9:
        return 0
    # COLUMNS
    s = [sol[i::9] for i in range(9)]
    s = [[x for x in l if l.count(x) > 1] for l in s]
    if s.count([]) != 9:
        return 0
    # Leave the 3x3 squares for what they are for now.
    # Now check if the binary operator constraints are satisfied.
    d = [dat[(i * 9):((i + 1) * 9)] for i in range(18)]
    for i in range(17):
        if i % 2 == 0:
            for j in range(8):
                if d[i][j] == 1 and sol[((i/2) * 9) + j] >= sol[((i/2) * 9) + j + 1]:
                    return 0
                if d[i][j] == 2 and sol[((i/2) * 9) + j] <= sol[((i/2) * 9) + j + 1]:
                    return 0
        else:
            for j in range(9):
                if d[i][j] == 1 and sol[((i/2) * 9) + j] >= sol[((i/2) * 9) + j + 9]:
                    return 0
                if d[i][j] == 2 and sol[((i/2) * 9) + j] <= sol[((i/2) * 9) + j + 9]:
                    return 0
    # Now if nothing went wrong we can safely return 1.
    return 1


# Function to give one GT-puzzle to the solver and return the results.
def minisat(i, am=0):
    # Copy the files in the correct folder. Prepare DIMACS.
    data = to_solver(i, am)
    # Now call for minisat.
    p = subprocess.check_output("C:\\cygwin\\bin\\minisat.exe C:\\sat\\d1.txt C:\\sat\\d2.txt",
                                stderr=subprocess.STDOUT)
    # Read out the information from STDOUT.
    p = sat_out(p)
    # Register difficulty.
    p[6] = data[0]
    # Read out the output file and check if the sudoku has been solved.
    p[7] = read_sat()
    # To be sure: Check whether the solution is valid.
    if p[0] == 1 and test_sol(p[7], data[1]) == 0:
        print('Solution to puzzle', i, 'is not correct.')
    elif p[0] == 0:
        print('Puzzle', i, 'does not seem to be satisfiable.')
    # Return p as the results and the solution. We can later hand this to a file.
    return p

# Function to get the distribution of difficulties accross the data.
def dif_dist(am, fname):
    # File operations.
    dataPath = os.path.join(os.path.dirname(os.path.realpath('__file__')), 'dif' + str(fname) + '.txt')
    if os.path.exists(dataPath) == True:
        try:
            os.remove(dataPath)
            print('Old difs file removed.')
        except ValueError:
            print('Could not remove difs.txt. Perhaps you have no access?')
    # Read data.
    with open('data.txt', 'r') as f:
        nums = f.read()
    nums = nums.splitlines()
    nums = [int(x.split('\t')[1]) for x in nums]
    count = [0] * 7
    # difs = [1,2,3,4,5,6]
    difs = [1,2,9,9,5,6]
    c = 0
    for i in range(1, 61016):
        #If we have had 500 of this level, take it out.
        if 1000 in count:
            difs[count.index(1000) - 1] = 9
            count[count.index(1000)] = -1
        # Do a solve if we still have to do this dif level.
        if nums[i] in difs:
            #Solve the sudoku.
            p = minisat(i+1, am)
            #Process the data.
            p = [str(x) for x in p]
            p = [re.sub('[,\[\]\ ]', '', x) for x in p]
            p = ' '.join(p)
            # Write to file.
            with open(dataPath, 'a+') as f:
                f.write(p + '\n')
            # Increase respective counter.
            count[nums[i]] += 1
            if c % 200 == 0:
                print('progress:', c)
            c += 1
        # Break if we have had all the cases.
        if difs.count(9) == 6:
            print('terminated')
            break
    return 0

# p = subprocess.check_output("C:\\cygwin\\bin\\minisat.exe C:\\sat\\d1.txt C:\\sat\\d2.txt",
#                                 stderr=subprocess.STDOUT)
# sat_out(p)


st = time.time()
dif_dist(0, 1)
dif_dist(27, 2)
dif_dist(54, 3)
dif_dist(81, 4)
end = time.time()
print(end-st)

# with open('difs.p', 'r') as f:
#     p = pickle.load(f)


# p = [1, 2, 160, 646, 11733, 0.046, 1, [8, 6, 7, 2, 5, 3, 4, 9, 1, 3, 9, 2, 8, 1, 4, 6, 7, 5, 5, 1, 4, 9, 6, 7, 3, 8, 2, 2, 8, 9, 1, 3, 5, 7, 4, 6, 7, 3, 5, 4, 2, 6, 8, 1, 9, 6, 4, 1, 7, 8, 9, 5, 2, 3, 1, 2, 6, 5, 4, 8, 9, 3, 7, 4, 7, 3, 6, 9, 2, 1, 5, 8, 9, 5, 8, 3, 7, 1, 2, 6, 4]]


# a = 1
# p = minisat(2)
# print(p)






#
# sol = [2, 9, 4, 7, 5, 8, 3, 1, 6, 3, 1, 6, 4, 9, 2, 5, 7, 8, 7, 5, 8, 6, 3, 1, 9, 4, 2, 5, 3, 2, 9, 8, 7, 4, 6, 1, 4, 8, 7, 3, 1, 6, 2, 9, 5, 9, 6, 1, 2, 4, 5, 7, 8, 3, 6, 4, 3, 1, 2, 9, 8, 5, 7, 8, 7, 9, 5, 6, 3, 1, 2, 4, 1, 2, 5, 8, 7, 4, 6, 3, 9]
# dat = [1, 2, 0, 2, 1, 0, 2, 1, 0, 1, 2, 1, 2, 1, 2, 1, 1, 1, 2, 1, 0, 1, 2, 0, 1, 1, 0, 1, 1, 1, 1, 2, 2, 1, 2, 2, 2, 1, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 2, 2, 0, 1, 2, 0, 2, 1, 1, 2, 2, 2, 2, 1, 1, 1, 2, 0, 2, 1, 0, 1, 2, 0, 1, 2, 2, 2, 1, 2, 1, 2, 2, 2, 2, 0, 1, 1, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 1, 1, 0, 2, 1, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 1, 0, 1, 2, 0, 1, 1, 0, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 0, 2, 2, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
# test_sol(sol, dat)

# print(p)


# p = subprocess.call("C:\\cygwin\\bin\\minisat.exe C:\\sat\\d1.txt C:\\sat\\d2.txt")
# C:\\sat\\d1.txt C:\\sat\\d2.txt

print('YOU ARE TERMINATED!')