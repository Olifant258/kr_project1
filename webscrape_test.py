import requests as reqs
import re
import os
import time

#Function to make a longer ID number in the form of a string.
def long_id(i):
    l = 0
    while i >= 1:
        i /= 10.0
        l += 1
    i = int(round(i * (10 ** l)))
    z = '0000'
    idl = z[:5-l] + str(i)
    return idl

#Function to obtain all data in the range [m,n]. If we want all data give no inputs.
# I tested the performance of the function today and it seems the bottleneck
# is obtaining data from the web. It take up 96% of the time and makes the
# overall function really slow. I recommend not trying it with all 60K puzzles.
def obtain_data(m = 1, n = 61017):
    #Remove data file if one exists already.
    dataPath = os.path.join(os.path.dirname(os.path.realpath('__file__')), 'data.txt')
    if os.path.exists(dataPath) == True:
        try:
            os.remove(dataPath)
            print('Old data file removed.')
        except ValueError:
            print('Could not remove data.txt. Perhaps you have no access?')
    #These are used to reduce the difficulty to value in the range [1,7].
    diffs = ['Super easy', 'Very easy', 'Easy', 'Medium', 'Hard', 'Harder', 'Very hard']
    #Loop over the URLs and obtain its puzzle.
    for x in range(m,n):
        #Print the progress every once in a while.
        if x - m % 5000 == 0 and x != m:
            print('Scraped', x - m, 'puzzles of the site.')
        #Obtain the puzzle from the web.
        url = 'http://www.menneske.no/sudokugt/eng/showpuzzle.html?number=' + str(x)
        r = reqs.get(url)
        #Now pre-process the content as given ny r.
        c = r.content
        c = c.splitlines()
        #Only remain with the lines about the lay-out of the sudoku.
        start = c.index('<h3>Sudoku puzzle</h3>')
        end = c.index('<!-- footer -->')
        c = [c[i] for i in range(start+3, end-7) if c[i]]
        #Some stuff to later put the sudoku structure in.
        s = []
        dif = ''
        #If we end up with a weird amount of lines, print the sudoku_ID.
        if c.__len__() != 99:
            print("Format does not match up")
        #Isolate the words that inidcate structure.
        else:
            for i in range(0,98):
                if c[i] == '</tr>' or c[i] == '<tr class="grid2">':
                    continue
                temp = re.sub('[/.]', ' ', c[i])
                s.append(temp.split()[3])
            #Obtain puzzle difficulty.
            if c[-1].split()[4] == 'Super' or c[-1].split()[4] == 'Very':
                dif = c[-1].split()[4] + ' ' + c[-1].split()[5]
            else:
                dif = c[-1].split()[4]
            dif = diffs.index(dif)
        #Now put the structure of the puzzle in our own format.
        #The idea is to have a list or string of integers corresponding to operators between squares.
        #Thus:  list[1:8] = the operators on the first row.
        #       list[9:16] = the operators below the first row (connecting row 1 to row 2). ETC.
        #We could use: 0 = no operator, 1 = smaller than, 2 = greater than.
        s_new = [0] * 81 * 2
        s = [list(e[2:]) for e in s]
        for i in range(9):
            for j in range(9):
                #Loop over the amount of operators adjacent to this square..
                for k in range(len(s[9 * i + j])):
                    if s[9 * i + j][k] == 'T':
                        s_new[2 * i * 9 + j - 9] = 2
                    elif s[9 * i + j][k] == 'R':
                        s_new[2* i * 9 + j] = 1
                    elif s[9 * i + j][k] == 'B':
                        s_new[2 * i * 9 + j + 9] = 1
                    elif s[9 * i + j][k] == 'L':
                        s_new[2 * i * 9 + j - 1] = 2
        #Put the ID, the difficulty and the puzzle structure in one list.
        #result = [x, dif, s_new] #Removed for now.
        #Now, I am not sure what's convenient for writing this to a file.
        #I'll opt to: make a long string, divided by tabs.
        result = long_id(x) + '\t' + str(dif) + '\t' + ''.join(str(e) for e in s_new)
        #Write that to the file.
        with open('data.txt', 'a+') as f:
            f.write(result + '\n')
    return 0

#Funtion to obtain one puzzle from the data file.
def open_data(i):
    #We can select 1 line, because each line has the same size.
    #The offset is measured in bytes from the beginning of the file.
    with open('data.txt', 'r') as f:
        data = f.read()
        data = data.splitlines()[i]
    return data

def open_data2(i):
    #We can select 1 line, because each line has the same size.
    #The offset is measured in bytes from the beginning of the file.
    with open('data.txt', 'r') as f:
        f.seek((i - 1) * 172)
        line = f.readline()
    return line

#Function to process the data back to a list with integers.
def process_data(line):
    data = line.split('\t')[2] # [:-1]
    data = [int(ele) for ele in data]
    data = [int(line.split('\t')[1]), data]
    return data

#Function to process the data back to a list with integers.
def process_data2(line):
    data = line.split('\t')[2][:-1]
    data = [int(ele) for ele in data]
    data = [int(line.split('\t')[1]), data]
    return data

#Function to print the sudoku in a really really ugly way. Maybe find a better way?
def ugly_print(f):
    #Print the top of the sudoku.
    print(' ___ ___ ___ ___ ___ ___ ___ ___ ___')
    #Loop over the 18*9 sized list. This contains 0,1,2 for nothing, smaller than and greater than respectively.
    for i in range(18):
        #If i % 2 == 0, we are talking about the operators in the same row.
        if i % 2 == 0:
            line = '|'
            for j in range(9):
                if f[i * 9 + j] == 0:
                    line += '   |'
                elif f[i * 9 + j] == 1:
                    line += '   <'
                elif f[i * 9 + j] == 2:
                    line += '   >'
            print(line + '   ' + str(i / 2 + 1))
        #If i % 2 == 1, we are talking about the operators from the upper row on the row below.
        else:
            line = ' '
            for j in range(9):
                if f[i * 9 + j] == 0:
                    line += '___ '
                elif f[i * 9 + j] == 1:
                    line += '_^_ '
                elif f[i * 9 + j] == 2:
                    line += '_v_ '
            print(line)
    return 0


#Function to retrieve a single sudoku from the data file.
def getData(i):
    data = process_data(open_data(i-1)) # data is a list[level, sudoku]
    # ugly_print(data[1])
    #print('DATA LOADED!')
    return data

#Function to retrieve a single sudoku from the data file.
def getData2(i):
    data = process_data2(open_data2(i))
    return data

# Function to get the distribution of difficulties accross the data.
def dif_dist():
    with open('data.txt', 'r') as f:
        data = f.read()
    data = data.splitlines()
    data = [int(x.split('\t')[1]) for x in data]
    dist = [data.count(x) for x in range(7)]
    print(dist)
    return 0


if __name__ == '__main__':
    # start = time.time()
    # obtain_data()
    # end = time.time()
    # print('Elapsed time:', end - start)

    dif_dist()

    print('YOU ARE TERMINATED!')